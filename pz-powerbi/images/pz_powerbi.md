# Практическое задание. Служба отчетов Power BI

## Задание

1. [Загрузить](https://disk.yandex.ru/d/o2J36XVcyxcocQ) и установить сервер Microsoft Power BI (PowerBIReportServer.exe) по [инструкции](https://docs.microsoft.com/ru-ru/power-bi/report-server/install-report-server).
1. [Загрузить](https://powerbi.microsoft.com/ru-ru/desktop/) и установить приложение Power BI Desktop (PBIDesktopRS_x64.msi или PBIDesktopRS.msi).
1. В приложении Power BI Desktop создать отчет с использованием данных Microsoft Analysis Services по [инструкции](https://docs.microsoft.com/ru-ru/power-bi/report-server/quickstart-create-powerbi-report).
1. Подготовить отчет о выполнении задания.

## Примечание
Для корректной установки **Microsoft Power BI** нужно предварительно установить [Microsoft .NET Framework 4.8](https://disk.yandex.ru/d/moMrm6jQZIwgWA) или воспользоваться файлом в каталоге \\SERVER732K\Public\7343\Для ТУДа.

## Ход  работы
### Задание 1 - Установка сервера Microsoft Power BI
1. Для корректной установки **Microsoft Power BI** нужно предварительно установить [Microsoft .NET Framework 4.8](https://disk.yandex.ru/d/moMrm6jQZIwgWA)

2. [Загрузить](https://disk.yandex.ru/d/o2J36XVcyxcocQ) и установить сервер Microsoft Power BI (PowerBIReportServer.exe) по [инструкции](https://docs.microsoft.com/ru-ru/power-bi/report-server/install-report-server).

![t1-1](images/1.PNG)

![t1-2](images/2.PNG)

![t2-3](images/3.PNG)


### Задание 2 - Установка приложения Power BI Desktop
[Загрузить](https://www.microsoft.com/ru-ru/download/details.aspx?id=105944)  и установить приложение Power BI Desktop (PBIDesktopRS_x64.msi или PBIDesktopRS.msi). Настройки все по умолчанию.

![t2-1](images/4.PNG)

![t2-2](iamges/5.PNG)

### Задание 3 - Создание отчета с использованием данных Microsoft Analysis Services
Всё сделано по [инструкции](https://docs.microsoft.com/ru-ru/power-bi/report-server/quickstart-create-powerbi-report).

![t3-1](images/6.PNG)

Пример воронки:

![t3-2](images/7.PNG)

## Заключение
В рамках данного задания была выполнена установка сервера Microsoft Power BI Report Server и приложения Power BI Desktop, а также создан отчет с использованием данных Microsoft Analysis Services. Процесс включал несколько этапов, каждый из которых был детально описан и проиллюстрирован скриншотами.


Отчет должен содержать:
1. Титульный лист
2. Задание, сформулированное преподавателем (оформляется отдельным списком).
3. Описание проделанной работы (описание должно быть достаточным для повторения действий неподготовленным человеком).
4. Скриншоты и листинги, подтверждающие самостоятельность выполненных действий.
